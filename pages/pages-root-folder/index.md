---
layout: frontpage
header:
  image_fullwidth: header_poly2.png
  title: Privacy by Design Foundation
permalink: /index.html
translations:
  en: /en
homepage: true
image:
  title: pbd.png
widget1:
  title: Yivi voor beschermde post (files en e-mails)
  url: https://postguard.eu/
  image: postguard-logo.png
  text: "Het open source PostGuard project biedt versleuteling en digitale ondertekening van bestanden en e-mails. Via Yivi wordt gegarandeerd dat alleen de beoogde ontvanger de verstuurde post kan lezen. Ook is duidelijk wie de afzender is, via een diagitale handtekening."
widget2:
  title: Gemeente Nijmegen breidt Yivi inlog uit
  url: https://www.nijmegen.nl/diensten/privacy/yivi-nieuwe-manier-van-inloggen/
  image: wethouder-van-elferen-open-yivi.jpeg
  text: "Wethouder van Elferen lanceert meer mogelijkheden voor burgers om met Yivi in te loggen, juist om privacy-vriendelijker met hun gegevens om te gaan."
widget3:
  title: Handelsregistergegevens nu in Yivi
  url: https://organisatiegegevens.signicat.nl/
  image: kvk-logo.jpeg
  text: "Een boost voor de digitale economie via zekerheid over online transacties en zaken doen: wie is bevoegd voor welke organisatie, via eenvoudige check. Bij deze uitgifte van attributen worden de kosten die de KvK vraagt voor de gegevens doorberekend."
---

### Stichting Privacy by Design

<img src="/images/pbd.png" style="float: right; width: 40%; padding: 15px" />

De stichting Privacy by Design creëert en onderhoudt gratis open
source software waarbij de privacy van de gebruiker voorop staat. Het
belangrijkste onderwerp van de stichting is het identity platform
[Yivi](/irma), zie ook de
aparte webpagina voor de [yivi app](https://yivi.app/). De stichting
wil ook in algemene zin de ontwikkeling en het gebruik van open,
privacy-vriendelijke en goed-beveiligde ICT bevorderen.

Met Yivi kunt u op een privacy-vriendelijke, beveiligde manier
eigenschappen (attributen) van uzelf onthullen (zoals: ik ben boven de
18), zonder dat u andere, niet-relevante informatie over uzelf
weggeeft. Via zulke attributen kunt u zichzelf authenticeren
bijvoorbeeld om in te loggen op een webpagina.

Ook kunt u met Yivi attribuut-gebaseerde handtekeningen zetten, zie
[Yivi in detail](/irma-uitleg) voor meer informatie.

<img src="/images/yivi-app-onboarding-nl.png" style="float: right; width: 20%; padding: 15px" />

Yivi is oorspronkelijk ontwikkeld onder de naam IRMA, binnen de
Radboud Universiteit. In die tijd is de naam niet wettelijk
beschermd. Met het toenemend gebruik van de app is het noodzakelijk om
de eigen naam goed te beschermen, om op te kunnen treden tegen
vervalsers. Daarom is in 2023 een nieuwe naam gekozen:
[Yivi](https://yivi.app/)


<p style="margin-bottom: 0;">De stichting Privacy by Design heeft twee
belangrijke operationele verantwoordelijkheden met betrekking tot
Yivi:</p>

1. Ontwikkeling en beheer van de [Yivi
software](http://github.com/privacybydesign).

2. Verspreiding van het Yivi gedachtengoed van een open, transparante,
privacy-vriendelijke, op publieke waarden gebaseerde digitale
identiteit.

## Samenwerking

De stichting Privacy by Design is in 2016 voortgekomen uit de [Digital
Security](http://www.ru.nl/ds/) en [iHub](https://ihub.ru.nl/)
onderzoeksgroepen van de Radboud Universiteit. De stichting is een
onafhankelijke non-profit spin-off.

De stichting werkt graag samen met anderen om Yivi van de grond te
krijgen. Pilots zijn in voorbereiding, in samenwerking met
verschillende andere partijen, waaronder in het bijzonder:

 * [SIDN](https://sidn.nl) voor beheer en de doorontwikkeling van Yivi,
   tot 1 april 2024, zie [persbericht](https://www.sidn.nl/nieuws-en-blogs/sidn-draagt-yivi-weer-over-aan-stichting-privacy-by-design).

 * de [Radboud Universiteit](https://www.ru.nl), voor (sterke)
   authenticatie van studenten en medewerkers en digitale
   ondertekening van bijv. tentamenuitslagen;

* [NLnet](https://nlnet.nl) voor ontwikkeling van een prototype voor
   identity-based encryption op basis van Yivi, via het *Next
   Generation Internet* programma NGI-PET, onder auspiciën van het *DG
   Communications Networks, Content and Technology* van de Europese
   Unie, zie de aankondiging [`User-friendly email encryption possible
   with identity-based
   cryptography'](https://www.ngi.eu/news/2019/08/20/user-friendly-email-encryption-possible-with-identity-based-cryptography/).

 * [SURF](https://www.surf.nl), voor uitgifte en controle van
   attributen via [SURFconext](https://www.surfconext.nl);

 * [Nuts](https://nuts.nl), voor een open infrastructuur in de zorg,
   zie ook de portal voor zorgverleners
   [Helder](https://helder.health/) waar men met Yivi
   [inlogt](https://helder.health/login).

 * [Alliander](http://www.alliander.nl), voor authenticatie en
   ondertekening in de context van `slimme' huizen. Daarnaast geeft
   Alliander de stichting structurele steun.

 * [Open Universiteit](https://www.ou.nl/), voor gezamenlijke Yivi
   software ontwikkeling.

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/ctz-GoIL6W0?start=217" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>
