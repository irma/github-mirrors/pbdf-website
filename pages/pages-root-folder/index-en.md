---
layout: frontpage
header:
  image_fullwidth: header_poly2.png
  title: Privacy by Design Foundation
permalink: /en/
language: en
translations:
  nl: /
homepage: true
image:
  title: pbd.png
widget1:
  title: Yivi for protected post (files and emails)
  url: https://postguard.eu/
  image: postguard-logo.png
  text: "The open source PostGuard project offers encryption and digital signing of files and emails. Via Yivi it is guaranteed that only the intended recipient can read the transferred content. Also, the identity of the sender is clear, via a digital signature."
widget2:
  title: Nijmegen municipality extends Yivi login
  url: https://www.nijmegen.nl/diensten/privacy/yivi-nieuwe-manier-van-inloggen/
  image: wethouder-van-elferen-open-yivi.jpeg
  text: "Alderman van Elferen launches more options for citizens to log in with Yivi, for better protection of their personal data."
widget3:
  title: Company data now in Yivi
  url: https://organisatiegegevens.signicat.nl/
  image: kvk-logo.jpeg
  text: "A boost for the digital economy via certainty about online business transactions: who is authorised for which organisation, via a simple check. At issuance of these company attributes the costs asked by the Dutch national KvK register are passed on."
  # url: /irma-en/
  # text: IRMA is the unique platform that makes you digitally <em>self-sovereign</em> and gives you full control over your personal data&#58; with IRMA on your phone you are empowered not only to prove who you are, but also to digitally sign statements.
  # image: irma-video-youtube.png
  # video: <a href="#" data-reveal-id="videoModal"><img src="/images/irma-video-youtube.png" width="302" height="182" alt=""/></a>
  # title: Cooperation with SIDN
  # url: https://www.sidn.nl/a/internet-security/why-would-you-share-more-data-than-you-need-to?language_id=2
  # image: SIDN-IRMA-signing-3-12-2018.jpg
  # text: The Privacy by Design foundation and the foundation SIDN that manages registration for the .nl domain have entered into an agreement to improve the usage and availability of IRMA.
  # title: Brouwer prize for IRMA
  # url: https://www.khmw.nl/brouwer-prijs-naar-privacy-by-design/
  # image: KHMW-logo.jpg
  # text: The Privacy by Design Foundation won the 2018 Brouwer prize for science and society, from the Royal Holland Society of Sciences and Humanities (KHMW). The jury commends IRMA's positive contribution to trust in society and its solid scientific basis.
---

### Privacy by Design Foundation

<img src="/images/pbd.png" style="float: right; width: 40%; padding: 15px" />

The Privacy by Design Foundation creates and maintains free and open
source software in which the privacy of the user is the most
important. The main application of the foundation is the identity
wallet [Yivi](/irma-en), see also the separate [Yivi
webpage](https://www.yivi.app/en). The foundation also aims to
generally improve the development and usage of open, privacy-friendly
and well-secured ICT.

With Yivi you can disclose properties (attributes) of yourself in a
privacy-friendly and secure way, for example, I am over 18 years old,
without disclosing other, non-relevant information about
yourself. Using such attributes you can authenticate yourself to for
example login on a website.

Additionally, with Yivi you can create attribute-based signatures, see
[Yivi in detail](/irma-explanation) for more information.

<img src="/images/yivi-app-onboarding-nl.png" style="float: right; width: 20%; padding: 15px" />

Yivi was originally developed under the name IRMA, within Radboud
University Nijmegen. At that time legal protection of the name IRMA
was not arranged. With the increasing usage of the app it became
necessary to protect the name and brand and to counter forgery and
falsification. Therefore, a new name was chosen, in 2023:
[Yivi](https://yivi.app/en).


<p style="margin-bottom: 0;">The foundation has two important
operational roles in IRMA:</p>

1. Development and supporting the [Yivi
software](http://github.com/privacybydesign).

2. Spreading the Yivi concept of open, transparant, privacy-friendly
digital identity based on public values.


<!-- #### Collaborations

The Privacy by Design foundation was set-up in 2016. It arose from the
[Digital Security](http://www.ru.nl/ds/) and
[iHub](https://ihub.ru.nl/) research groups of the Radboud University.
The foundation is an independent non-profit spin-off.

The foundation welcomes collaboration with others to get Yivi up and
running.  Pilots are now being prepared with the help of a number of
other parties, among which:

 * [SIDN](https://www.sidn.nl/?language_id=2) for maintaining and
   developing IRMA. IRMA becomes Yivi at April 4, 2023.

 * The [Radboud University](https://www.ru.nl/english/), for (strong)
   authentication of student and employees and for digital signing of,
   e.g., examination grades.

* [NLnet](https://nlnet.nl) for development of a prototype for
   identity-based encryption on the basis of IRMA, via the *Next
   Generation Internet* programma NGI-PET, under the aegis of the
   European Union's DG Communications Networks, Content and
   Technology, see the announcement [`User-friendly email encryption
   possible with identity-based
   cryptography'](https://www.ngi.eu/news/2019/08/20/user-friendly-email-encryption-possible-with-identity-based-cryptography/)

 * [SURF](https://www.surf.nl/en/services-and-products/surfconext/index.html),
   for issuing and verifying of attributes through SURFconext.

 * [Nuts](https://nuts.nl), for an open infrastructure in healthcare,
   see also the portal for medical doctors
   [Helder](https://helder.health/) with
   [logon](https://helder.health/logon) via IRMA.

 * [Alliander](http://www.alliander.nl), for authentication and
   signing within the context of `smart' homes. In addition, the
   foundation is structurally supported by Alliander.

 * [Open University](https://www.ou.nl/en/home), for joint IRMA
   software development.

The foundation receives external funding via the following projects.

 * *Connecting (with) IRMA* (2018), via the
    [SIDNfonds](https://www.sidnfonds.nl/projecten), see in particular
    this [project
    page](https://www.sidnfonds.nl/projecten/connecting-with-irma).

 * *Citizen Science* (2018-2020), via the European Regional
    Development Fund
    [ERDF](http://ec.europa.eu/regional_policy/en/funding/erdf/); see
    the [ERDF projecten in the province of
    Gelderland](https://www.europaomdehoek.nl/projecten/?radius=&projectProvince[]=Gelderland).

* *Chronical Pain* (2018-2020), via the European Regional Development
    Fund [ERDF](http://ec.europa.eu/regional_policy/en/funding/erdf/),
    see [ERDF projecten in the province of
    Gelderland](https://www.europaomdehoek.nl/projecten/?radius=&projectProvince[]=Gelderland).


<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/q6IihEQFPys?start=217" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="videoRI" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/vINtD58nLPQ" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>

-->