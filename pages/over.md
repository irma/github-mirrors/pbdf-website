---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: Over de stichting
meta_title: Over de stichting
teaser: De stichting Privacy by Design wil in algemene zin de ontwikkeling en het gebruik van open, privacy-vriendelijke en goed-beveiligde ICT bevorderen.
permalink: /over/
language: nl
translations:
  en: /about
---

Meer in het bijzonder richt de stichting zich op de ontwikkeling en
het gebruik van het eigen systeem [Yivi](/irma), oorspronkelijk IRMA
genaamd.  Met Yivi kunt u op een privacy-vriendelijke,
beveiligde manier eigenschappen (attributen) van uzelf onthullen
(zoals: ik ben boven de 18), zonder dat u andere, niet-relevante
informatie over uzelf weggeeft. Via zulke attributen kunt u zichzelf
*authenticeren* bijvoorbeeld om in te loggen op een webpagina.

Ook kunt u met Yivi attribuut-gebaseerde handtekeningen zetten,
zie [Yivi](/irma-uitleg) voor meer informatie.

### Verslagen en verantwoording

Het eerste jaarverslag, met daarin onder andere een overzicht van de
uitgeoefende activiteiten en een financiële verantwoording, is hier
te vinden. 

* [Jaarverslag 2023](/pdf/jaarverslag-2023.pdf)

* [Jaarverslag 2020](/pdf/pbdf_jaarverslag_2020.pdf)

* [Jaarverslag 2019](/pdf/pbdf_jaarverslag_2019.pdf)

* [Jaarverslag 2018](/pdf/jaarverslag-2018.pdf)

* [Jaarverslag 2017 en eind 2016](/pdf/jaarverslag-2017.pdf)
