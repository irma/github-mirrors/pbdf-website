---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: Toevoegen van Kamer van Koophandel attributen
permalink: /uitgifte-kvk/
language: nl
translations:
  en: /issuance-kvk
---

De [Kamer van Koophandel](https://www.kvk.nl/) beheert het
handelsregister, een van de basisregistraties van de Nederlandse
overheid. Ondernemingen, stichtingen en verenigingen staan (verplicht)
ingeschreven in het handelsregister, onder andere met hun: KvK nummer,
handelsnaam, vestigingsplaats, bevoegdheden, etc. Dit register biedt
zekerheid voor iedereen die zaken wil doen met een (onbekende)
organisatie.

Gegevens uit dit handelsregister kunnen toegevoegd worden in de Yivi
app, in de vorm van een KvK-kaartje met relevante gegevens. Deze
toevoeging (uitgifte) wordt uitgevoerd door het bedrijf
[Signicat](https://www.signicat.com/nl/) in opdracht van
[SIDN](https://www.sidn.nl/).  Er zijn eenmalige kosten verbonden aan
het ophalen van dit KvK-kaartje. Het kaartje is na uitgifte 5 jaar
geldig en kan zo vaak gebruikt worden als men wil.

Met dit KvK-kaartje kun je zelf bewijzen dat je bevoegd ben om namens
een organisatie te handelen. Hiermee kunnen allerlei transacties
digitaal met zekerheid uitgevoerd worden.

[Terug](/uitgifte) naar uitgifte van attributen.
