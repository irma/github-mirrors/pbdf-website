---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: Het toevoegen van een e-mailadres attribuut
permalink: /uitgifte-email/
language: nl
translations:
  en: /issuance-email
---

U kunt meerdere e-mailadressen toevoegen als attribuut in uw Yivi app,
bijv. uw zakelijke adres en ook uw privé adres.

Als eerste stap in het uitgifte-proces van een e-mail adres voert u uw
e-mailadres in de getoonde textbalk. Er wordt dan een
verificatie-email naar dit adres gestuurd, waarin een link staat.
Wanneer u op die link klikt ontvangt u het e-mail attribuut.

(Het blijkt dat in sommige e-mail lezers op mobiele telefoons deze
link niet goed "klikbaar" weergegeven wordt. U kunt dan proberen de
link zelf te kopiëren in een webbrowser. U kunt ook proberen de mail
op een ander apparaat, zoals een laptop of PC, te lezen. Er wordt naar
gekeken hoe dit probleem het beste opgelost kan worden.)

De geldigheid van dit e-mail attribuut is *een jaar*.

SIDN verwijdert dit e-mailadres na uitgifte uit de systemen.  In het
kader van onderhoud en monitoring houdt SIDN logs bij van uitgiftes.
Het logbestand wordt automatisch verwijderd na verloop van tijd.
Bovendien staat het e-mailadres hier niet in vermeld.

[Terug](/uitgifte) naar uitgifte van attributen.
