---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: Yivi demo's
permalink: /demo/
language: nl
translations:
  en: /demo-en
---

Deze pagina geeft uitleg over verschillende mogelijke toepassingen van
Yivi, om jezelf bekend te maken maar ook om zelf een digitale
handtekeningen te zetten.  Bij ieder van de onderstaande knoppen wordt
verdere uitleg gegeven. Gedetailleerde
[uitleg](https://creativecode.github.io/irma-made-easy/posts/putting-an-age-check-on-a-static-website-using-irma/)
voor ontwikkelaars is beschikbaar.



Login met e-mail adres
:   <a class="button" href="/demo/mail">E-mail login</a>

Yivi handtekeningen voor toestemmingen en verklaringen
:    <a class="button" href="/demo/ondertekenen">Yivi handtekening</a>

Adres opgave via automatisch invullen van velden
:    <a class="button"
href="/demo/adres">Adres invullen</a>

Verificatie of iemand student is
:    <a class="button"
href="/demo/student">Student check</a>

Leeftijdscontrole
:    <a class="button"
href="/demo/18plus">18+ check</a> &nbsp; <a class="button"
href="https://www.angrygames.nl/index.html">Angry Games demo</a>

Registratie en verificatie om online films te kijken
:    <a class="button"
href="/demo/irmaTube">YiviTube</a>

Geketende Yivi sessies
:    <a class="button"
href="/demo/irmaTubePremium">YiviTube Premium</a>

Verificatie of iemand in leven is
:    <a class="button"
href="/demo/attestatiedevita">Attestatie de vita demo</a>
