---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: Yivi meetings
meta_title: Meetings
permalink: /meetings/
language: nl
translations:
  en: /meetings
---

About two or three times per year a *Yivi meeting* is organised, free
of charge, open to everyone interested in identity management in
general and in Yivi in particular. The meetings usually draw between
25 and 50 participants, from industry, government and academia. The
atmosphere is open and very much content-oriented.

 * **Language:** mix of English and Dutch, depending on speaker and
     audience.

 * **Announcements:** via the IRMA/Yivi mailing list and via
   [Mastodon](https://mastodon.nl/@yivi_privacybydesign). If you like to be
   included (or deleted) from this list, please send an email to: b.jacobs
   'at' privacybydesign.foundation

### Yivi meeting: Vrijdagmiddag, 11 okt. 2024 bij SURF

**Programma** 

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00, Sara Vahdati Pour (student, Nijmegen), *Keyvi, single-signon with Keycloak login authenticator using Yivi* [[slides](../meeting-slides/slides-11-10-2024/sara-vahdatipour-sso.pdf)]

  3. 14:00 - 14:30, Robert van Altena (VerID), *Yivi in de verzekeringswereld* [[slides](../meeting-slides/slides-11-10-2024/robert-van-altena-yivi-verzekeringswereld.pdf)]

  4. 14:30 - 15:00 Jan den Besten, Lian Vervoort, Dirk Doesburg (Nijmegen), *Yivi voor PubHubs en PubHubs voor Yivi* [[slides van Jan en Lian](../meeting-slides/slides-11-10-2024/jan-den-besten-pubhubs.pdf)] [[slides van Dirk](../meeting-slides/slides-11-10-2024/dirk-doesburg-DIYivi.pdf)]

  5. 15:00 - 15:30 pauze

  6. 15:30 - 16:00, Sietse Ringers, *(Cryptographic) developments in EUDI wallets* [[slides](../meeting-slides/slides-11-10-2024/sietse-ringers-eudi-wallets.pdf)]

  7. 16:00 - 16:45, Martijn Sanders, Ivar Derksen (SIDN), Dibran Mulder (Caesar Groep), *Yivi transitie van SIDN naar Caesar* [[slides van Martijn en Ivar](../meeting-slides/slides-11-10-2024/martijn-sanders-yivi-handover.pdf)] [[slides van Dibran](../meeting-slides/slides-11-10-2024/dibran-mulder-yivi-caesar.pdf)]

<p style="text-align: center">
  <img src="../meeting-slides/slides-11-10-2024/yivi-hand-over-11-10-2024.jpg" width="500"/>
</p>


  9. 16:45: borrel, buiten SURF


### Yivi meeting: Vrijdagmiddag, 10 nov. 2023 bij SIDN

**Programma** 

De bijeenkomst begint om 13:30, bij
[SIDN](https://www.sidn.nl/over-sidn/contact). **Let op** de
bijeenkomst is deze keer bij SIDN in Arnhem, en dus **niet bij SURF**
in Utrecht, zoals gebruikelijk. De precieze locatie is: SIDN, Meander
501, 6825 MD Arnhem, Bedrijfsrestaurant 3e etage.

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00, Joost van Dijk (Yubico), "FIDO en webgebaseerde
  digital identity wallets" [[video](https://youtu.be/wP9-y1OSmY0?si=z4pQL0yfko0dLi1I)]

  3. 14:00 - 14:30, Theo Hooghiemstra (Hooghiemstra en Partners),
  *Authenticatie in de zorg* [[video](https://www.youtube.com/watch?v=5S4pHvoHZdk&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq&index=2)]

  4. 14:30 - 15:00 Peter Eikelboom (SIDN), *Europese en nationale
  identity ontwikkelingen* [[video](https://youtu.be/De9c_LLH_hQ?si=9hgmFhXA1bJk0lms)]

  5. 15:00 - 15:30 pauze

  6. 15:30 - 16:00, iHub team (Radboud), *Yivi handtekeningen voor
  files, mails & posts* [[video](https://youtu.be/B5xXb6XJWhY?si=eoa7OVbnLZMVXneP)]

  7. 16:00 - 16:30, Job Doesburg (Radboud), *Maatregelen tegen overvraging in 
  SSI en Yivi* [[video](https://www.youtube.com/watch?v=PHf2VbXaIe0)]

  8. 16:30 - 17:00, Martijn Sanders (SIDN), *Yivi ontwikkelingen* [[video](https://youtu.be/y9pRk6fCO1M?si=7bx8K_iUZsndsS2C)]

  9. 17:00: borrel, aangeboden door SIDN



### Yivi meeting: Vrijdagmiddag, 26 mei 2023.

**Programma** 

De bijeenkomst begint om 13:30, bij
[SURF](https://www.surf.nl/contact-en-support/contact-met-surf).

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00, Martijn van Dam (stichting datakluis), *De
  Nederlandse Datakluis*

  3. 14:00 - 14:45, Wendy van den Eeckhout en Marnix Dessing
  (gemeente Nijmegen), *Uitgifte en gebruik van attributen*

  4. 14:45 - 15:15 Sietse Ringers (ICTU), *De publieke
  voorbeeldwallet: techniek en context*

  5. 15:15 - 15:45 pauze

  6. 15:45 - 16:15 Job Doesburg (Radboud), *Maatregelen tegen overvraging*

  7. 16:15 - 16:45 Niels van Dijk (SURF), *SURF proof of concept met
  educatie wallet*

  8. 16:45 - 17:00 Martijn Sanders (SIDN), *State of the IRMA*

  9. 17:00: borrel, aangeboden door SURF


### IRMA meeting: Vrijdagmiddag, 3 februari 2023.

**Programma** 

De bijeenkomst begint om 13:30, bij
[SURF](https://www.surf.nl/contact-en-support/contact-met-surf).

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00 Wilbert Junte (Stichting CIS) en Bas Maat (Achmea),
  *IRMA & verzekeren* 

  3. 14:00 - 14:30 Tim Speelman (Beleidsmedewerker Digitale
  Identiteit, BZK), *Europese Digitale Identiteit en de publieke
  voorbeeldwallet* [[video](https://www.youtube.com/watch?v=tqwLjATve5Q&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq)]

  4. 14:30 - 15:00 Michiel Mayer (KvK) en Robert van Altena (VerID),
  *IRMA & KvK* [[video](https://www.youtube.com/watch?v=ka8sjmsH3ZA&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq)]

  5. 15:00 - 15:30 Pauze

  6. 15:30 - 15:45 Gijs Nijman (Joindata), *Bedrijfsidentificatie in
  de agrisector* 

  7. 15:45 - 16:00 Jos Kuijpers (Lead software developer, OnePlanet)
  *IRMA & OpenPlanet Data Platform* 

  8. 16:00 - 16:30 Martijn Sanders (SIDN), *State of the IRMA*
  [[video](https://www.youtube.com/watch?v=_IzUOome1E4&list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq)]

  10. 16:30 Borrel


### Vorige IRMA meeting: Vrijdagmiddag, 30 september 2022.

**Programma** 

De bijeenkomst begint om 13:30, bij
[SURF](https://www.surf.nl/contact-en-support/contact-met-surf).

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00 Marlies Rikken, Niels van Dijk (SURF), *eduID en
  SSI volgens SURF* [[video](https://www.youtube.com/watch?v=3ABM234pNzo)]

  3. 14:00 - 14:45 Laurens Debackere (Digitaal Vlaanderen), *An
  introduction to identity & authentication in the Flemish Solid
  ecosystem* [[video](https://www.youtube.com/watch?v=jZIzLmnBB98)]

  4. 14:45 - 15:00 Daniel Ostkamp (RU), *Ontwikkeling Postguard email
  versleuteling* [[video](https://www.youtube.com/watch?v=6qHOe0ADd4I)]

  5. 15:00 - 15:30 Pauze

  6. 15:30 - 16:00 Bart Kerver (VWS), *UZI-pas ontwikkelingen*
  [[video](https://www.youtube.com/watch?v=b-FPdFIgaoE)]

  7. 16:00 - 16:15 Jan den Besten (RU),
  *IRMA in het nieuwe community netwerk PubHubs*
  [[video](https://www.youtube.com/watch?v=HY7SyIHnIck)]

  8. 16:15 - 16:30 Koen de Jonge (Procolix),
  *Met IRMA de cloud in*
  [[video](https://www.youtube.com/watch?v=Kchnxjfa51I)]

  9. 16:30 - 17:00 Martijn Sanders en Ivar Derksen (SIDN), *State of
  the IRMA* [[video](https://www.youtube.com/watch?v=AAD6bJSRkho)]

  10. 17:00 Drinks


### Vorige IRMA meeting: Vrijdagmiddag, 6 mei 2022.

**Programma** 

De bijeenkomst begint om 13:30, bij
[SURF](https://www.surf.nl/contact-en-support/contact-met-surf).

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00 Joost Fleuren (Kamer van Koophandel), *KVK
  bevoegdheden en IRMA: waarom en hoe?*
  [[video](https://www.youtube.com/watch?v=IYckupZ3w10)]

  3. 14:00 - 14:30 Allard Keuter (Signicat), *IRMA via de Signicat
  broker, voor eIDs in NL en EU*
  [[video](https://www.youtube.com/watch?v=tWdr-UrN6Hg)]

  4. 14:30 - 15:50 Daniel Ostkamp, Leon Botros (Radboud Universiteit),
  *Encrypted e-mail met IRMA, als minimal lovable product*
  [[video](https://www.youtube.com/watch?v=tonNQQSPHiM)]

  5. 15:50 - 15:10 Mike Alders (Gemeente Amsterdam), *Je
  identiteitsdocument in IRMA*. Als je je paspoort of ID-kaart
  meeneemt, kun je het zelf ook proberen.
  [[video](https://www.youtube.com/watch?v=bPbg98nhdzI)]

  6. 15:10 - 15:45 Pauze

  7. 15:45 - 16:15 Ingrid Rijper, Anne van Doorn (StichtingCIS),
  *Ervaring met IRMA voor toegang tot centraal gedeelde
  verzekeringsgegevens*
  [[video](https://www.youtube.com/watch?v=-BtVH4kigaE)]

  8. 16:15 - 16:30 Wout Slakhorst (Nedap / Nuts), *Machtigen met KvK
  credential* [[video](https://www.youtube.com/watch?v=GBcvpWHZ-NI)]

  10. 16:30 - 17:00 Martijn Sanders, Sietse Ringers (SIDN), *State of
  the IRMA* [[video](https://www.youtube.com/watch?v=8uWx5lzQvE0)]

  11. 17:00 - 18:00 Borrel, aangeboden door SURF.



### Earlier IRMA meeting: Friday afternoon, November 5, 2021.

**Programme** 

De bijeenkomst begint om 13:30, in zaal 002 op Drift 25, van de
Universiteit van Utrecht, op loopafstand van Centraal Station
Utrecht. 

  1. 13:30 Opening, welkom

  2. 13:30 - 14:00 Remko Hoekstra en Ewout van Haeften (Centraal
     InkoopBureau, CIB). *Landelijke uitrol van de Gehandicapten
     Parkeer App: fraudebestrijding met behulp van IRMA*, zie ook
     [GPA](https://www.gpkapp.nl/zo-werkt-het). [[slides](../meeting-slides/slides-5-11-21/hoekstra-vanhaeften-gpa.pdf)]
     [[video](https://www.youtube.com/watch?v=Imrh_5nMSfU)]

  3. 14:00 - 14:30 David Lamers en Joris Lange (Rabobank), *Deep dive
     & learnings from Datakeeper*, zie ook
     [Datakeeper](https://datakeeper.nl/). [[slides](../meeting-slides/slides-5-11-21/lamers-lange-datakeeper.pdf)]
     [[video](https://www.youtube.com/watch?v=QdIaK2X_5DM)]

  4. 14:30 - 15:00 Pauze 

  5. 15:00 - 15:30 Dennis Bor en Lian Vervoort (Radboud Universiteit),
  *Twid: tegengaan van desinformatie op Twitter met
  IRMA*. [[slides](../meeting-slides/slides-5-11-21/vervoort-bor-twid.pdf)]
  [[video](https://www.youtube.com/watch?v=KrXMINnrQHY)]

  6. 15:30 - 16:00 Sietse Ringers en Martijn Sanders (SIDN), *State of
     the IRMA*
     [[slides](../meeting-slides/slides-5-11-21/ringers-sanders-state.pdf)]
     [[video](https://www.youtube.com/watch?v=Y0SqLTmqtv4)]

  7. 16:00 - 16:30 Peter Havekes (SURF - Trust and Identity), *Using
     IRMA as an alternative to in person identity
     validation*. [[slides](../meeting-slides/slides-5-11-21/havekes-vermeulen-rv.pdf)]
     [[video](https://www.youtube.com/watch?v=pl5Ho3llf9k)]


Ter afsluiting is er informeel napraten in [The Streetfood
Club](https://thestreetfoodclub.nl/), Janskerkhof 9, Utrecht.



### Earlier IRMA meeting: Friday afternoon, June 25, 2021.

**Programme** 

Alle
[video links](https://www.youtube.com/playlist?list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq)
van deze bijeenkomst.

*This meeting will be in Dutch*. De video link is
[premium.irma-meet.nl/irma-talks](https://premium.irma-meet.nl/irma-talks). 

Deze videodienst maakt gebruik van
[BigBlueButton](https://bigbluebutton.org/), dat in principe in elke
browser moet werken. Heb je toch problemen, probeer dan Google's
Chrome browser. Die lijkt het soepelst te werken, op alle platformen.

   1. 14:00 Opening, welkom

   2. 14:05 - 14:25 Marieke Brouwer (Gemeente Groningen) en Marloes
van Unen (Gemeente Amsterdam), *IRMA-vote en pilots met
burgerparticipatie in Groningen en Amsterdam* [[video](https://www.youtube.com/watch?v=dDxw3Csd_2I)]

   3. 14:25 - 14:40 Leon Botros en Daniel Ostkamp (RU), *Versleutelde
e-mail met IRMA: eerste prototype* [[video](https://www.youtube.com/watch?v=B6NDA1Ouq1k)]

   4. 14:40 - 14:55 Rowan Goemans en Laura Loenen (RU), *Cryptify:
   versleutelde file transfer met IRMA* [[video](https://www.youtube.com/watch?v=s3deWHy1uSc)]

   5. 14:55 - 15:00 Koffie / thee / plas pauze

   6. 15:00 - 15:20 Martijn van Dijk (RU, nu Gemeente Nijmegen),
*Authenticiteit van berichten en hun afzender via IRMA handtekeningen
als instrument tegen desinformatie*
[[video](https://www.youtube.com/watch?v=gb9qUjCHV04)]

   7. 15:20 - 15:40 Reinder Rustema
   ([petities.nl](https://petities.nl/)), *Petities en initiatieven
   steunen met IRMA* [[video](https://www.youtube.com/watch?v=ku5kxsoa2D4)]

   8. 15:40 - 16:00 Ivar Derksen (SIDN), *State of the IRMA*
   [[video](https://www.youtube.com/watch?v=OeT9WUs6Zsk)]


This online meeting takes place via
[IRMA-meet](https://premium.irma-meet.nl/irma-talks). Registration is
not required. The video clips of the separate talks are made available
below. You can participate passively, without appearing in any
recording, and anonymously. But if you actively participate, for
instance by engaging in discussions and/or switching on your camera,
you may become part of the recording. Your choice to actively
participate is taken as consent to record your participation and make
it public via the recorded video.

**Meeting etiquette, for participants**

   * The meeting is hosted via a special version of the authenticated
     irma-meet video service. For this special version you can choose
     to either reveal your official name (from the
     [BRP](https://services.nijmegen.nl/irma/gemeente/start)) or to
     use a self-chosen name. In the latter case you can participate
     anonymously. Please avoid choosing offensive names.

   * Keep your microphone muted during the meeting, unless you wish to
     ask a question or give a comment. Use the chat to communicate to
     the host your wish to engage.  Please be brief and to the point
     in your remarks/questions.

   * You can also use the chat for general feedback or communication
     to other participant. Please post only short and relevant
     remarks.

   * Keep also your video camera off, in order to minimise traffic.

The host of the meeting may intervene, and ultimately exclude you from
the meeting, in case you do not follow this etiquette or disrupt the
meeting in any other way.


### Earlier IRMA meeting: Friday afternoon, March 5, 2021.

**Programme** 

*This meeting will be in Dutch*. De video link is
[premium.irma-meet.nl/irma-talks](https://premium.irma-meet.nl/irma-talks). 

Deze videodienst maakt gebruik van
[BigBlueButton](https://bigbluebutton.org/), dat in principe in elke
browser moet werken. Heb je toch problemen, probeer dan Google's
Chrome browser. Die lijkt het soepelst te werken, op alle platformen.

Alle
[video links](https://www.youtube.com/playlist?list=PL4oSeW0AbF-kf39Wk5CKZ_D2RiHJQnNsq)
van deze bijeenkomst.

   1. 14:00 Opening

   2. 14:05 Steven Gort (ICTU), "IRMA en Waardepapieren -- Visie op
   attribuut gebasseerde credentials i.r.t. Publieke Dienstverlening"
   [[slides](../meeting-slides/slides-5-3-21/Kennissessie-IRMA-en-Waardepapieren.pdf)] [[video](https://www.youtube.com/watch?v=lfWGalGOXXs)]

   3. 14:25 Joost Fleuren (Innovatielab KvK), "Know Your Customer
   (KYC) check met delen bevoegdheden en machtigingen attributen,
   i.s.m. Ivido"
   [[slides](../meeting-slides/slides-5-3-21/KVK-InnovatieLab-KYC.pdf)]
   [[video](https://www.youtube.com/watch?v=eJuFp9tBDVw)]

   3. 14:45 Henk Dieter Oordt (Tweede Golf) en Lisa Bosma
   (Drechtsteden), "ID-contact: betere dienstverlening door jouw
   gemeente met meer grip op je gegevens"
   [[slides](../meeting-slides/slides-5-3-21/ID-Contact-IRMA-meetup.pdf)]
   [[video](https://www.youtube.com/watch?v=DqiHoqWpi5c)] [[demo
   video](https://vimeo.com/519943741)]

   4. 15:05 Marc De Jong (BZK), "Ontwikkelingen Digitale Overheid"
   [[video](https://www.youtube.com/watch?v=lLodiKAWSPc)]

   5. 15:20 Pauze

   6. 15:30 Ineke van Gelder en Mike Alders (Team Digitale Identiteit
   Amsterdam), "Updates onderzoeken & next steps met IRMA in
   Amsterdam"
   [[slides](../meeting-slides/slides-5-3-21/MijnAmsterdam-en-filmpje.pdf)]
   [[video](https://www.youtube.com/watch?v=WyMgVToU5Cw)] [[video Hoe
   werkt IRMA](https://www.youtube.com/watch?v=iuGt-5UnNZs)]

   7. 15:50 Bernard van Gastel (Radboud Universiteit), "Locaal stemmen
   met IRMA" [[video](https://www.youtube.com/watch?v=wK-Aq4wDkw8)]

   8. 16:10 Koen de Jonge (Procolix), "IRMA-meet"
   [[video](https://www.youtube.com/watch?v=PGd78YDhMuQ)]

   9. 16:25 Patrick Terranea (Zynyo), "IRMA-authenticatie voor
   document ondertekening"
   [[video](https://www.youtube.com/watch?v=7LyLsoIR1CE)]

   10. 16:40 Sietse Ringers (SIDN), "Updates IRMA ontwikkelingen"
   [[slides](../meeting-slides/slides-5-3-21/State-of-the-IRMA.pdf)]
   [[video](https://www.youtube.com/watch?v=PDACEbWLXVY)]

   11. 16:55 Afsluiting

   12. 17:00 Eind




### **Cancelled:** IRMA meeting: Friday afternoon, July 3, 2020.

Due to Covid-19 restrictions this meeting is postponed until after the
summer.

<!--- 

**Programme:** 

   1. 13:30 - 14:00 "Gebruiksproeven met de nieuwe IRMA interface",
      Mark Fonds, Gemeente Amsterdam.

   2. 14:00 - 14:30 "State of the IRMA", Sietse Ringers en Tomas Harreveld,
      Privacy by Design Foundation.

   3. 14:30 - 15:00 "Remote identity vetting voor enrolment met IRMA,
      iDIN en ReadID", Pieter van der Meulen, SURFnet

      15:00 - 15:30 Koffie / thee

   4. 15:30 - 16:00 "Authenticatie met IRMA binnen het ministerie van
      justitie en veiligheid: work in progress", Bernd Keuning, JustId.

   5. 16:00 - 16:30 "Van een technologische silo benadering naar een
      'samenlevingsarchitectuur'", Steven Gort, ICTU

   6. 16:30 - 17:00 "DigieSign met IRMA", Patrick Terranea, Zynyo.

**Drinks:**

   17:00 - 18:00, offered by SURF; requires registration, see below
	
**Registration:** Participation is free, but requires registration, by
sending an email to: dis-secr 'at' cs.ru.nl with your name and
intention to join. Drinks are only available if you register on or
before **April 1**.

--->



### Earlier IRMA meeting: Friday afternoon, November 29, 2019.

**This meeting is not in Utrecht**. This time it will be organised by
the city of Amsterdam, at **Weesperstraat 113**, from 13:30 -
17:30.

**Afternoon programme:** 
Several general presentations about IRMA and its applications

   1. 13:30 - 14:00 "State of the IRMA", Sietse Ringers en Tomas
      Harreveld, Privacy by Design Foundation. [[slides](../meeting-slides/slides-29-11-19/ringers-harreveld-irma.pdf)]

   2. 14:00 - 14:30 "Inwoners centraal bij het ontwerp van nieuwe
      diensten", Mark Fonds, Ineke van Gelder, Mike Alders, 
      Gemeente Amsterdam. [[slides](../meeting-slides/slides-29-11-19/Amsterdam-Inwonerscentraal-ontwerpen-ontwikkelen.pdf)]

   3. 14:30 - 15:00 "Proef met DigiD Bronidentiteit in combinatie met
      IRMA", Hans Rob de Reus, Ministerie van Binnenlandse Zaken.

   15:00 - 15:30 Coffee / tea break

   4. 15:30 - 16:00 "Veilig Bellen", Alexander Blom, Bloqzone

   5. 16:00 - 16:30 "Proof of Concept Patientauthenticatie met IRMA",
      Marcel Settels Vereniging van Zorgaanbieders voor
      Zorgcommunicatie (VZVZ). [[slides](../meeting-slides/slides-29-11-19/VZVZ_patientauthenticatie_POC.pdf)]

   6. 16:30 - 17:00 "Identity based encryption of email with IRMA",
      Wouter Geraedts, Privacy by Design Foundation. [[slides](../meeting-slides/slides-29-11-19/IRMAseal-presentation.pdf)]

   17:00 - 17:30 Drinks

**Registration:** Participation is free, but registration is required
for logistic reasons if you wish to enjoy the drinks, by sending an
email to: dis-secr 'at' cs.ru.nl, before **Nov. 26**.


### Earlier IRMA meeting: Friday afternoon, July 5, 2019.

**Afternoon programme:** 
Several general presentations about IRMA and its applications

   1. 13:30 - 14:00 "Quick introduction to IRMA and overview of new
      developments", Sietse Ringers, [Privacy by Design
      Foundation](https://privacybydesign.foundation) and Open
      University. [[slides](../meeting-slides/slides-5-7-19/ringers-harreveld-5-juli-2019.pdf)]

   2. 14:00 - 14:30 "Toepassingen van IRMA voor Persoonlijke
      Gezondheidsomgevingen (PGOs), bij Ivido en Medmij", Tristan
      Garssen, [Ivido](https://www.ivido.nl). [[slides](../meeting-slides/slides-5-7-19/garssen-5-juli-2019.pdf)]

   3. 14:30 - 15:00 "Privacy-by-Design en SIDN samen op weg naar:
      Garantie op stabiliteit van de IRMA Backbone", Michiel Graat,
      [SIDN](https://www.sidn.nl). [[slides](../meeting-slides/slides-5-7-19/graat-5-juli-2019.pdf)]

   4. 15:00 - 15:30 Coffee / Tea

   5. 15:30 - 16:00 "Een gebruiksvriendelijke gegevensmanager voor je
      online identiteit", Mark Fonds, [Gemeente
      Amsterdam](https://www.amsterdam.nl/). [[slides](../meeting-slides/slides-5-7-19/fonds-5-juli-2019.pdf)]

   6. 16:00 - 16:30 "IRMA pilots bij verschillende gemeenten", Tomas
      Harreveld, [Privacy by Design
      Foundation](https://privacybydesign.foundation). [[slides](../meeting-slides/slides-5-7-19/ringers-harreveld-5-juli-2019.pdf)] (dezelfde als hierboven voor het eerste praatje van Ringers).

**Drinks:**

   16:30 - 17:30, offered by SURF; requires registration, see below
	
**Registration:** Participation is free, but registration is required
for logistic reasons if you wish to enjoy the drinks, by sending an
email to: dis-secr 'at' cs.ru.nl, before **July 3**.


### Earlier IRMA meeting: Friday, March 8, 2019.

**Morning programme:**

   10:00 - 12:30 Technical workshop for developers

**Lunch:**

   12:30 - 13:30, offered by SURF; requires registration, see below

**Afternoon programme:** 
Several general presentations about IRMA and its applications

   1. 13:30 - 14:00 "Quick introduction to IRMA and overview of new
      developments", Sietse Ringers, [Privacy by Design
      Foundation](https://privacybydesign.foundation) and Open
      University
	 [[slides](../meeting-slides/slides-8-3-19/ringers-8-maart-2019.pdf)]

   2. 14:00 - 14:30 "Machtiging via IRMA bij VGZ", Niels Scheurleer,
	[VGZ](https://www.vgz.nl)
	 [[slides](../meeting-slides/slides-8-3-19/scheurleer-8-maart-2019.pdf)]

   3. 14:30 - 15:00 "Authenticatie voor Persoonlijk
	Gezondheidsomgevingen (PGOs)", Wout Slakhorst, 
	[Nedap](https://nedap.com) / [Nuts](https://nuts.nl)
	 [[slides](../meeting-slides/slides-8-3-19/slakhorst-8-maart-2019.pdf)]

   4. 15:00 - 15:30 Coffee / Tea

   5. 15:30 - 16:00 "Back-up and recovery of IRMA attributes", Ivar
        Derksen, master student, Radboud University
	 [[slides](../meeting-slides/slides-8-3-19/derksen-8-maart-2019.pdf)]

   6. 16:00 - 16:30 "Attribuut-uitgifte door gemeenten: status en
         plannen", Bram Withaar, Gemeente
         [Nijmegen](https://nijmegen.nl)
	 [[slides](../meeting-slides/slides-8-3-19/withaar-8-maart-2019.pdf)]

**Drinks:**

   16:30 - 17:30, offered by SURF; requires registration, see below
	
**Registration:** Participation is free, but requires registration, by
sending an email to: I.Haerkens 'at' cs.ru.nl. Lunch and drinks are
only available if you register on or before **March 5**.  Please
include in your email:

I will participate in the:

 * morning technical programme: Yes / No
 * lunch: Yes / No
 * afternoon talks programme: Yes / No
 * drinks: Yes / No.


### Earlier IRMA meeting: Friday, June 1, 13:30 -- 17:30, 2018.

**Programme:**

   1. 13:30 - 14:15 "Introduction to IRMA and overview of new
      developments" Sietse Ringers, Privacy by Design Foundation and
      Open University
      [[slides](../meeting-slides/slides-1-6-18/sietse-ringers-irma-developments.pdf)]

   2. 14:15 - 15:00 "IRMA signatures: sign-by-QR and sign-by-mail"
      Koen van Ingen, Alliander
      [[slides](../meeting-slides/slides-1-6-18/koen-van-ingen-signatures.pdf)]

   3. 15:00 - 15:30 Tea/coffee break

   4. 15:30 - 16:00 "IRMA and blockchains"
      Timen Olthof, Alliander
      [[slides](../meeting-slides/slides-1-6-18/timen-olthof-irma-schemes-blockchain.pdf)]

   5. 16:00 - 16:30 "IRMA for municipalities: issuance and
      verification of attributes" Bram Withaar, Gemeente Nijmegen
      [[slides](../meeting-slides/slides-1-6-18/bram-withaar-nijmegen-issuance.pdf)]

   6. 16:30 - 16:45 Open discussion about strategies/next steps. Bart
      Jacobs, Privacy by Design Foundation and Radboud University.

   7. 16:45 - 17:30 Drinks


### Earlier IRMA meeting: Friday, Nov. 3, 10:00 - 13:00, 2017.

**Programme:**

   1. 10.00 - 10:30 Oscar Koeroo (KPN information security office).
      "Security for Remote SIM Provisioning for e-SIM"

      Abstract: Traditionally phones use SIM cards to authenticate
      subscribers to the mobile network. The SIM cards contain
      information about the home network and several authentication
      keys of the mobile network and the subscriber. Manufacturers are
      miniaturising the SIM cards to reduce costs and aid
      applicability in Phones and accessories. The step beyond the
      nano-SIM card form-factor is the complete removal of the SIM
      card itself and replacing it with an e-SIM, or embedded
      SIM. With this embedded SIM mobile network operators can load
      the same SIM card information dynamically into a phone. This
      opens up a new attack surface, by virtue of new components in
      mobile devices, new provisioning protocol as alternative to the
      physical SIM card. It also has geo-political implications,
      depending upon the trust model. And depending upon the
      architecture and assuming we also include IoT with e-SIMs it
      could also introduce a new business continuity risk to 60B
      devices globally, as per prospect of the adoption of this
      technology in the foreseeable future.

   2. 10:30 - 11:00 Pim Vullers (NXP).  "IRMA prototype on a NXP
      SmartMX3 P71 smart card"

   3. 11:00 - 11:30 Pauze

   4. 11:30 - 12:00 Marie-José Hoefmans (Schluss). "The Schluss
      initiative", see
      [schluss.org](https://www.schluss.org/) for back ground information.
      [[slides](../meeting-slides/slides-3-11-17/schluss.pdf)]

   5. 12:00 - 12:15 Stijn Meijer (Radboud University en ING)
      "Comparing uPort's blockchain-based identity management with
      IRMA" see [uport.me](https://www.uport.me/)

   6. 12:15 - 12:45 Sietse Ringers (Radboud Universiteit) "Latest IRMA
      developments", see
      [privacybydesign.foundation](https://privacybydesign.foundation/en)
      [[slides](../meeting-slides/slides-3-11-17/irma-developments-nov17.pdf)]

   7. 12:45 Lunch

      In case you wish to enjoy the sandwhich lunch, you need to
      register by sending an email to: I.Haerkens 'at' cs.ru.nl,
      before Wednesday nov. 1.


