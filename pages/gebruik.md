---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: IRMA gebruik
permalink: /gebruik/
language: nl
translations:
  en: /usage

---

Zie hier een lijstje waar IRMA nu toegepast wordt.

Zorgsector

  * [Ivido](https://platform.ivido.nl/) een persoonlijke gezondheidsomgeving
    (PGO) met inloggen via IRMA.

  * [MIJNPGO](https://mijnpgo.org/) een andere persoonlijke
    gezondheidsomgeving (PGO) met IRMA-login.

  * Huisartspraktijken [Medipark Uden](https://medipark.hix365.nl/) en
    [1e lijn](https://1elijn.praktijkinfo.nl/onlinepatientomgeving/)
    en [Snelder](https://mijn.huisartsenpraktijksnelder.nl/),
    ondersteund door [Chipsoft](https://www.chipsoft.nl).

    Zie ook de eigen uitleg [pagina](https://magazine.chipsoft.nl/irma-installeren-online/welkom/) en [video](https://www.youtube.com/watch?v=6OSS0REAb0g) van Chipsoft voor het gebruik van IRMA.

  * Patiëntenportaal [Fonkelzorg](https://fonkelzorg.nl/patientenportaal/)

  * [Helder.health](https://helder.health/) voor artsen om toegang te
    krijgen tot medische dossiers van pati&euml;nten.

Gemeentes

  * [IRMA: nieuwe manier van
    inloggen](https://www.amsterdam.nl/innovatie/digitalisering-technologie/irma-nieuwe-manier-inloggen/) demo website van gemeente Amsterdam.

  * [ID-bellen](https://www.idbellen.nl/) voor (mobiel) bellen met je
    gemeente waarbij bellers eerst met IRMA bewijzen wie ze zijn.

  * [IRMA-vote](https://www.ru.nl/ihub/research/research-projects/irma-vote/)
    voor locaal stemmen met IRMA, voor het vergroten van
    burgerparticipatie.

Universiteiten

  * [Surfdrive online
    opslag](https://www.surf.nl/nieuws/pilot-surfdrive-voor-studenten)
    voor studenten, na authenticatie met IRMA.

Verzekeringen

  * Inzage in de eigen verzerkeringsgegevens bij [Stichting
    CIS](https://www.stichtingcis.nl/).

Digitale handtekeningen

  * [030 IRMA](https://www.030irma.nl/) voor het toevoegen van
    persoonsgegevens aan een pdf document.

Corona

  * [QRona](https://qrona.info/) voor het zelf registreren van 
    bezoekers, tegen corona.

  * [Demo Covid test uitslagen](https://demo.irma.dev/).

IRMA intern

  * [MijnIRMA](https://privacybydesign.foundation/mijnirma/) voor het
    beheer van de eigen IRMA app en inzien van het gebruik ervan.

  * [IRMA-meet](https://irma-meet.nl/) voor videobellen waarbij de
    deelnemers eerst moeten bewijzen wie ze zijn.

    

Deze lijst is ongetwijfeld niet volledig. Laat het vooral weten als er
(stabiele) IRMA toepassingen zijn die hier ontbreken (op: irma 'at'
privacybydesign.foundation).

Op een aparte pagina zijn ook [demo's](/demo) van het gebruik van IRMA
beschikbaar.

Onze *preferred partners* 

* voor ontwikkeling en integratie van IRMA-toepassingen: [Tweede
  Golf](https://tweedegolf.nl/)

* voor user experience (UX) / design: [informaat](https://informaat.nl/nl)

* voor managed hosting: [ProcoliX](https://www.procolix.com/)




