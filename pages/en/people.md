---
layout: page
meta_title: People
header:
  image_fullwidth: header_poly2.png
  title: People at the foundation
permalink: /people/
language: en
translations:
  nl: /mensen
---

The board of the Privacy by Design foundation consists of the people
listed below. They all fulfill their role in the board in a personal
capacity and are not being paid for it.

 * **Chairman**: Bart Jacobs (b.jacobs 'at' privacybydesign.foundation)

   [Bart Jacobs](http://www.cs.ru.nl/~bart) is a full professor of
   computer security at Radboud University in Nijmegen, The
   Netherlands. He works, together with his research group, on many
   societally relevant security and privacy topics, such as medical
   privacy, public transport travel cards, electronic voting.

 * **Secretary**: Monique Hennekens

   Monique Hennekens is a judge at the court of appeal of
   Arnhem-Leeuwarden. Before that, Monique was an attorney-at-law,
   specialised in privacy and IT law. Monique helps the Privacy by
   Design foundation with the legal aspects regarding Yivi.

 * **Treasurer**:  Jean Popma (j.popma 'at' privacybydesign.foundation) 

   [Jean Popma](https://www.linkedin.com/in/jeanpopma) works as
   project manager Applied Security Research at Radboud University
   Nijmegen; in that role he is responsible for the realization of
   privacy-friendly systems for the storage and sharing of scientific
   medical date. He has ample experience as ICT-manager and as
   Security Officer within Radboud University.

 * [Martijn van der Linden](https://nl.linkedin.com/in/mmjvdlinden) is
   Teamleider Quality and Development of the municipality Oss. Earlier
   he worked for the city of Nijmegen where he was closely involved
   with the pioneering Yivi (at the time: IRMA) developments there.

 * [Tim Vermeulen](https://www.linkedin.com/in/timverm/) (t.vermeulen
   'at' privacybydesign.foundation) is IT R&D Manager at network
   company Alliander. His teams works on embedding innovative IT
   solutions in the energy landscape. This involves techniques like
   Blockchain, Augmented Reality en Continuous Delivery platforms. Tim
   focuses on using Yivi technology within the energy domain.



<!-- ### Developers

Sietse Ringers (s.ringers 'at' privacybydesign.foundation) is *chief
architect* and *lead developer* of the Privacy by
Design foundation. In addition, the following people are (or have
been) active in developing software and web-pages of the foundation.

#### Active developers

* Sietse Ringers
* Ivar Derksen
* Maja Reißner
* David Venhoek
* Leon Botros
* Bart Jacobs

#### Former developers

* Hanna Schraffenberger
* Fabian van den Broek
* Tomas Harreveld
* Ayke van Laethem
* Koen van Ingen
* Joost van Dijk
* [Maarten Everts](https://nn8.nl/)
* Wouter Lueks
* Roland van Rijswijk-Deij
* Pim Vullers
* Bas Westerbaan
 
-->