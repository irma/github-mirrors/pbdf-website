---
layout: page
meta_title: Support
header:
  image_fullwidth: header_poly2.png
  title: Support the Foundation
permalink: /support/
language: en
translations:
  nl: /steun
---

The Privacy by Design foundation develops and maintains the IRMA
app for privacy-friendly authentication and signing. This happens
without profit and without abuse of personal data.

Feel free to [contact](/contact-en) us for support or cooperation.
