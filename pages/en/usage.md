---
layout: page
header:
  image_fullwidth: header_poly2.png
  title: IRMA usage
permalink: usage/
language: en
translations:
  nl: /gebruik

---

Below is a list of places where IRMA is currently used.

Health sector

  * [Ivido](https://platform.ivido.nl/) a personal health environment with
    login via IRMA.

  * [MIJNPGO](https://mijnpgo.org/) anoter personal health environment
    with IRMA-login.

  * General Practioners [Medipark Uden](https://medipark.hix365.nl/)
    en [1e
    lijn](https://1elijn.praktijkinfo.nl/onlinepatientomgeving/) en
    [Snelder](https://mijn.huisartsenpraktijksnelder.nl/), supported
    by [Chipsoft](https://www.chipsoft.nl).

  * Patient portal [Fonkelzorg](https://fonkelzorg.nl/patientenportaal/)

  * [Helder.health](https://helder.health/) for medical doctors to
    get access to the dossiers of patients.

Municipalities

  * [IRMA: nieuwe manier van
    inloggen](https://www.amsterdam.nl/innovatie/digitalisering-technologie/irma-nieuwe-manier-inloggen/) demo website of the city of Amsterdam.

  * [ID-bellen](https://www.idbellen.nl/) for (mobile) calling with your
    municipality where callers first prove who they are with IRMA.

  * [IRMA-vote](https://www.ru.nl/ihub/research/research-projects/irma-vote/)
    for local voting IRMA, to increase citizen participation.

Universities

  * [Surfdrive online
    storage](https://www.surf.nl/en/news/pilot-surfdrive-for-students)
    for students, af authentication with IRMA.

Insurances

  * Access to your own insurance data at [Foundation
    CIS](https://www.stichtingcis.nl/); so far available in Dutch only.

Digital signatures

  * [030 IRMA](https://www.030irma.nl/) for adding personal data to a
    pdf document.

Corona

  * [QRona](https://qrona.info/) for registering your visitors, against
    COVID-19.

  * [Demo Covid test results](https://demo.irma.dev/).

IRMA internally

  * [MijnIRMA](https://privacybydesign.foundation/mijnirma/) for
    managing ones own IRMA app and for inspecting its usage.

  * [IRMA-meet](https://irma-meet.nl/) for video calling where 
    participants first have to prove who they are.

Undoubtedly, this list is not complete. Please inform us about missing
(stable) IRMA applications (at: irma 'at' privacybydesign.foundation).

There is a separate page for IRMA [demos](/demo-en).

Our *preferred partners* 

* for development and integration of IRMA-applications: [Tweede
  Golf](https://tweedegolf.nl/)

* for user experience (UX) / design: [informaat](https://informaat.nl/en)

* for managed hosting: [ProcoliX](https://www.procolix.com/)

